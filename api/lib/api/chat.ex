defmodule Api.Chat do
  use Ecto.Schema
  import Ecto.Changeset


  schema "chats" do
    field :handle, :string
    field :message, :string

    timestamps()
  end

  @doc false
  def changeset(chat, attrs) do
    chat
    |> cast(attrs, [:handle, :message])
    |> validate_required([:handle, :message])
  end
end
