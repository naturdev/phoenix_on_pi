defmodule ApiWeb.ChatView do
  use ApiWeb, :view

  def render("index.json", %{chats: chats}) do
    %{
      chats: Enum.map(chats, &chat_json/1)
    }
  end

  def render("show.json", %{chat: chat}) do
    %{ chat: chat_json(chat)}
  end

  def chat_json(chat) do
    %{
      handle: chat.handle,
      message: chat.message,
      inserted_at: chat.inserted_at
    }
  end

end
