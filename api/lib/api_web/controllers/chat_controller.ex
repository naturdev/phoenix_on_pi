defmodule ApiWeb.ChatController do
  use ApiWeb, :controller

  alias Api.Chat

  def index(conn, _params) do
    chats = Api.Repo.all(Chat)
    render conn, "index.json", chats: chats
  end

  def create(conn, chat_params) do
    changeset = Chat.changeset(%Chat{}, chat_params)

    case Api.Repo.insert(changeset) do
      {:ok, chat} ->
        render(conn, "show.json", chat: chat)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Chat.ChangesetView, "error.json", changeset: changeset)
    end
  end

end
